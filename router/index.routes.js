import express from 'express';
export const router = express.Router();

router.get("/", (req, res) => {
    res.render("index", {
    });

    router.get("/index", (req, res) => {
        const params = {
            // inputRecibo: req.query.inputRecibo,
            // inputNombre: req.query.inputNombre,
            // inputDomicilio: req.query.inputDomicilio,
            // inputServicio: req.query.inputServicio,
            // inputKilowatts: req.query.inputKilowatts
        };
        res.render("index", params);
    });

    router.post("/index", (req, res) => {
        const params = {
            inputRecibo: req.body.inputRecibo,
            inputNombre: req.body.inputNombre,
            inputDomicilio: req.body.inputDomicilio,
            inputServicio: req.body.inputServicio,
            inputKilowatts: req.body.inputKilowatts
        };
        res.render("salida", params);
    });

    router.get("/salida", (req, res) => {
        const params = {
            inputRecibo: req.query.inputRecibo,
            inputNombre: req.query.inputNombre,
            inputDomicilio: req.query.inputDomicilio,
            inputServicio: req.query.inputServicio,
            inputKilowatts: req.query.inputKilowatts
        };
        res.render("salida", params);
    });

});

export default { router };